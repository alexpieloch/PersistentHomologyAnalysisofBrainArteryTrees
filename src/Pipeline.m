%% Initialize the workspace
clear all;
close all;
clc;


%% Load in persistence diagrams and case information (age,gender)

repoPath = fileparts(pwd);
load([repoPath,'/data/PersistenceDiagrams/H0PersistenceDiagrams.mat']);
load([repoPath,'/data/PersistenceDiagrams/H1PersistenceDiagrams.mat']);
load([repoPath,'/data/PersistenceDiagrams/CaseInformation.mat']);


%% Add file paths for MarronMatlab7

marronRoot = '....../Downloads/AllMatlab7Combined'; %% SET CORRECT DIRECTORY PATH TO SOFTWARE PACKAGE MENTIONED IN README.txt 
addpath([marronRoot,'/BatchAdjust']);
addpath([marronRoot,'/Smoothing']);
addpath([marronRoot,'/General']);


%% Run PCA representation of persistence diagrams

PCARepresentation(H0Array,Ages,Sexes,1,100);clc

PCARepresentation(H1Array,Ages,Sexes,1,100);clc


%% Run age analysis of persistence diagrams

[H0_age_rho, H0_age_pval] = AgeAnalysis(H0Array,Ages,Sexes,1,100);clc

[H1_age_rho, H1_age_pval] = AgeAnalysis(H1Array,Ages,Sexes,1,100);clc


%% Run gender analysis of persistence diagrams

[H0_sex_pval] = GenderAnalysis(H0Array,Ages,Sexes,1,100);clc

[H1_sex_pval] = GenderAnalysis(H1Array,Ages,Sexes,1,100);clc


%% Display results

fprintf('Pearson Correlation of H0 Features vs Age:         %d\n',    H0_age_rho);
fprintf('Pearson p-value of H0 Features vs Age:             %d\n\n',  H0_age_pval);

fprintf('Pearson Correlation of H1 Features vs Age:         %d\n',    H1_age_rho);
fprintf('Pearson p-value of H1 Features vs Age:             %d\n\n',  H1_age_pval);

fprintf('Permutation Test p-value for Sex with H0 Features: %d\n\n',  H0_sex_pval); 

fprintf('Permutation Test p-value for Sex with H1 Features: %d\n\n',  H1_sex_pval);

