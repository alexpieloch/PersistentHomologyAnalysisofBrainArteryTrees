function [ rho, pval ] = AgeAnalysis( dgmArray, Ages, Sexes, start, stop )


% Preprocess data
[ mdata, vAgeWDatas, vSexWDatas, vrange, colmap, markerstr ] = PreprocessData(dgmArray, Ages, Sexes, start, stop);


%  Make PC1 vs. PC2 Scatterplot
figure;
paramstruct = struct('npc',2, ...
                     'iscreenwrite',1, ...
                     'viout',[0 0 0 0 1]) ;
outstruct = pcaSM(mdata,paramstruct) ;
mpc = getfield(outstruct,'mpc') ;
paramstruct = struct('icolor',2, ...
                     'markerstr',markerstr, ...
                     'xlabelstr','PC 1 Scores', ...
                     'ylabelstr','PC 2 Scores', ...
                     'labelfontsize',12, ...
                     'iscreenwrite',1) ;
projplot2SM(mpc,eye(2),paramstruct) ;


%  Make PC1 vs. age Scatterplot
figure;
paramstruct = struct('npc',1,...
                     'iscreenwrite',1,...
                     'viout',[0 0 0 0 1]) ;
outstruct = pcaSM(mdata,paramstruct) ;
vscores = getfield(outstruct,'mpc') ;

hold on ;
for  iCaseWD = 1:length(mdata(1,:)) ;
  plot(vAgeWDatas(iCaseWD),vscores(iCaseWD),markerstr(iCaseWD), ...
                'Color',colmap(iCaseWD,:)) ;

end ;
vax = axisSM(vAgeWDatas,vscores) ;
axis(vax) ;

[rho,pval] = corr(vAgeWDatas',vscores','type','Pearson') ;
text(vax(1) + 0.05 * (vax(2) - vax(1)), ...
     vax(3) + 0.94 * (vax(4) - vax(3)), ...
     ['Pearson Correlation = ' num2str(rho)], ...
     'FontSize',12) ;
text(vax(1) + 0.05 * (vax(2) - vax(1)), ...
     vax(3) + 0.88 * (vax(4) - vax(3)), ...
     ['p-val = ' num2str(pval)], ...
     'FontSize',12) ;

xlabel('Age (years)','FontSize',12) ;
ylabel('PC1 Scores','FontSize',12) ;
hold off ;



end

