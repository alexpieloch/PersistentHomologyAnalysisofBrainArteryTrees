function [ ] = PCARepresentation( dgmArray, Ages, Sexes, start, stop )

% Preprocess data
[ mdata, vAgeWDatas, vSexWDatas, vrange, colmap, markerstr ] = PreprocessData(dgmArray, Ages, Sexes, start, stop);


%  Do curvDat Analysis
figure;
paramstruct = struct('vipcplot',[0 1 2 3], ...
    'vicolplot',[1 3 4], ...
    'icolor',2, ...
    'iscreenwrite',1) ;
curvdatSM(mdata,paramstruct) ;

subplot(4,3,2) ;
cla ;
vmean = mean(mdata,2) ;
plot(vrange',vmean,'k-') ;
vax = axisSM(vmean) ;
axis([1 length(vrange) vax(1) vax(2)]) ;
title('Mean') ;

subplot(4,3,3) ;
cla ;
ylabel('') ;
mresid = mdata - vec2matSM(vmean,size(mdata,2)) ;
vax = axisSM(mresid) ;
hold on ;

for i = 1:length(mdata(1,:)) ;
    plot(vrange',mresid(:,i),'-','Color',colmap(i,:)) ;
end ;
hold off ;
axis([1 length(vrange) vax(1) vax(2)]) ;

subplot(4,3,5) ;
vachil = get(gca,'Children') ;
set(vachil(1),'String','')

subplot(4,3,8) ;
vachil = get(gca,'Children') ;
set(vachil(1),'String','')

subplot(4,3,11) ;
vachil = get(gca,'Children') ;
set(vachil(1),'String','')

end

