Repository containing brain tree data, persistence diagrams and statistical analysis pipeline for the paper:
Persistent homology analysis of brain artery trees 
Paul Bendich, J.S. Marron, Ezra Miller, Alex Pieloch, and Sean Skwerer

The directory /src contains all the needed functions to statistically analyze the persistence diagrams as done in the above paper.  See the function Pipeline.m to see an example of how to run the entire analysis.

The directory /data/PersistenceDiagrams contains both the 0-dimensional persistence diagrams and the 1-dimensional persistence diagrams for each patient analyzed.  It also contains a mat file which has the age and sex information/labels for each patient analyzed.

The directory /data/OriginalBrainTreeData contains the original brain artery trees for each patient.  See the specific README.txt in the directory for more information.

Note: To run this pipeline one will need to download a copy of the following software package and set the correct directory path on line 17 of Pipeline.m:

http://stat-or.unc.edu/webspace/miscellaneous/marron/Matlab7Software/AllMatlab7Combined.zip

To generate persistence diagrams from the brain tree data we used the publicly available software package TDATools.  The software package is available at:

https://gitlab.com/paulbendich/tdatoolstrial



For any questions please contact at alexpieloch@gmail.edu
