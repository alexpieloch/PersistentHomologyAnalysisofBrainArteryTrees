This project is about analyzing ``brain artery trees'' using persistent homology and statistics.

In the res directory is stored about 100 decorated_tree.aca.Case###.mat files. Each one represents the arterial structure of the brain of human subject.
More precisely, it represents the output of a ``tube-tracking'' algorithm that took MRA images as input.

For more details on the original MRA image dataset, please see:

 http://www.insight-journal.org/midas/community/view/21

For details on the tube-tracking algorith, please see the following paper by Bullit and Aylward, among others:

http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.102.7885

Each brain corresponds to an (anonymous) human subject. The information retained on each subject (age, gender, handedness) is stored
in res/subjectdata.mat

In subjectdata.mat, handedeness is encoded as follows:

Right handed: 1
Left handed: 2
Ambidextrous: 3
Left/Ambidextrous: 4
Right/Ambridextrous: 5

Gender is encoded as follows:

Female: 1
Male: 2
Male -> Female: 3
Female -> Male: 4

The brain tree structure of subject N is stored in decorated_tree.aca.CaseN.mat, here are further details.
This is a struct, with four top fields:

case-num (obvious)

V. This is a 1 X (number of vertices) struct; there are usually around 120K vertices per tree.
The location field in V{k,1} stores the 3-D coordinates of the kth vertex. For details on the rest of the fields in V, please email
sskwerer@email.unc.edu; note that none of these fields are relevant at the moment to this project.

Branch. This is a 1 X (number of branches) struct; there are usually around 50 branch vertices per tree.
Details are as above for V.

E. This is a (number of edges) X 2 array. This is the edge list of the tree, where the prescence of (k,l) in the array
indicates an edge between vertices k and l. Note that most vertices are degree two, some are higher.

In BasicTreeInfo.mat, Contains a structure with fields:

subjectID: obvious
totalLength: the total vessel length in the brain artery tree


For more information on the project itself, please contact Paul Bendich (bendich@math.duke.edu). 